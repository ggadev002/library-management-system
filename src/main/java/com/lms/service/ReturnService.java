package com.lms.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lms.domain.Checkout;
import com.lms.repository.ReturnRepository;

@Service
public class ReturnService {
	@Autowired
	private ReturnRepository returnRepository;

	@Transactional
	public List<Checkout> findBorrowedItems(String registrationId) {
		return returnRepository.findBorrowedItems(registrationId);
	}

	@Transactional
	public boolean returnItem(Checkout checkout) {
		return (returnRepository.save(checkout) == null);
	}

	@Transactional
    public Checkout save(Checkout checkout) {
        return returnRepository.save(checkout);
    }
}
