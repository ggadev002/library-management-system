package com.lms.service;

import com.lms.domain.User;

public interface UserService {
	User authenticateUser(String username, String password);
}
