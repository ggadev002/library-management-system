package com.lms.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lms.domain.LibraryMember;
import com.lms.repository.MemberRepository;

@Service
public class MemberService {

	@Autowired
	private MemberRepository memberRepository;


	@Transactional
	public boolean saveMember(LibraryMember memberData) {

		//User user = userRepository.findByUsernameAndPassword(username, password);
		return (memberRepository.save(memberData) == null);
	}

	@Transactional
	public List<LibraryMember> findByNameContaining(String name) {
		return memberRepository.findByNameContaining(name);
	}

	public List<LibraryMember> findAll() {
	    return memberRepository.findAll();
	}


	@Transactional
	public List<LibraryMember> findByRegistrationIdContaining(String id) {
		return memberRepository.findByRegistrationIdContaining(id);
	}
}
