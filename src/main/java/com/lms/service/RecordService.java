package com.lms.service;

import java.util.List;

import com.lms.domain.collection.Record;

public interface RecordService {
    List<Record> searchByIsbn(String isbn);
    List<Record> searchByTitle(String title);
    Record save(Record record);
    Record findById(Integer id);

}
