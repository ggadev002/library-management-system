package com.lms.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lms.domain.Checkout;
import com.lms.repository.CheckoutRepository;

@Service
public class CheckoutServiceImpl implements CheckoutService {

	@Autowired
	private CheckoutRepository checkoutRepository;

    @Override
    public Checkout save(Checkout checkout) {
        return checkoutRepository.save(checkout);
    }


}
