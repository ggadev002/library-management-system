package com.lms.service;

import com.lms.domain.Checkout;

public interface CheckoutService {
    Checkout save(Checkout checkout);

}
