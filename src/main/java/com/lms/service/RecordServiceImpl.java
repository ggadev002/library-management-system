package com.lms.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lms.domain.collection.Record;
import com.lms.repository.RecordRepository;

@Service
public class RecordServiceImpl implements RecordService {

	@Autowired
	private RecordRepository recordRepository;

    @Override
    public List<Record> searchByIsbn(String isbn) {
        return recordRepository.findAllByIsbnContaining(isbn);
    }

    @Override
    public List<Record> searchByTitle(String title) {
        return recordRepository.findAllByTitleContaining(title);
    }

    @Override
    public Record save(Record record) {
        return recordRepository.save(record);
    }

    @Override
    public Record findById(Integer id) {
        return recordRepository.findOne(id);
    }


}
