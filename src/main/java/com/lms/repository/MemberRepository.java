package com.lms.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.lms.domain.LibraryMember;

public interface MemberRepository extends JpaRepository<LibraryMember, Integer> {

	List<LibraryMember> findByRegistrationId(String registrationId);

	List<LibraryMember> findByRegistrationIdContaining(String registrationId);

	@Query("select l from LibraryMember l WHERE l.info.firstName LIKE %:name% OR l.info.lastName LIKE %:name%")
	List<LibraryMember> findByNameContaining(@Param(value = "name") String name);

}
