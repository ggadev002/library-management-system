package com.lms.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lms.domain.collection.Record;

public interface RecordRepository extends JpaRepository<Record, Integer>{

	List<Record> findAllByIsbnContaining(String isbn);
	List<Record> findAllByTitleContaining(String title);

}
