package com.lms.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lms.domain.Checkout;

public interface CheckoutRepository extends JpaRepository<Checkout, Integer>{
}
