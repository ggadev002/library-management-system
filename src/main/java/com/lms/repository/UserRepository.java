package com.lms.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lms.domain.User;

public interface UserRepository extends JpaRepository<User, Integer>{

	User findOneByUsernameAndPassword(String username, String password);
}
