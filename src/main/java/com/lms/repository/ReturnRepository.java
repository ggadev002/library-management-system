package com.lms.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.lms.domain.Checkout;

public interface ReturnRepository extends JpaRepository<Checkout, Integer>{

	@Query("select c from Checkout c WHERE c.borrower.registrationId=:registrationId AND c.returnDate is null")
	List<Checkout> findBorrowedItems(@Param(value = "registrationId") String registrationId);
}
