package com.lms.gui;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;

import com.lms.domain.User;
import com.lms.service.UserService;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;

public class Login extends Presentation {

	@FXML
	private TextField username;

	@FXML
	private PasswordField password;

	@FXML
	private Label lblStatus;

	@Autowired
	private UserService userService;

	User user;


	public Login(ScreensConfig config) {
		super(config);
	}

	@FXML
	void doLogin(ActionEvent event) throws IOException {
		// verify user with db
		User user = userService.authenticateUser(username.getText(), password.getText());
		if (user != null) {
		    config.setUserSession(new UserSession(user));
			config.loadDashboardForm();
		} else {
			lblStatus.setText("Invalid username & password!");
			lblStatus.setTextFill(Color.web(config.ERROR_COLOR_CODE));
		}

	}

	@FXML
	void doExit() {
	    System.exit(0);
	}

	@FXML
	void initialize() {

	}
}
