package com.lms.gui.field;

import java.util.Date;

public class CheckoutField {

    private Integer colSn;
	private Integer colCheckoutId;
	private Date colCheckoutDate, colDueDate;
	private Integer colMemberID;
	private String colTitle, colBorrower;
	private String colCheckoutBy;
    public Integer getColCheckoutId() {
        return colCheckoutId;
    }
    public void setColCheckoutId(Integer colCheckoutId) {
        this.colCheckoutId = colCheckoutId;
    }
    public Date getColCheckoutDate() {
        return colCheckoutDate;
    }
    public void setColCheckoutDate(Date colCheckoutDate) {
        this.colCheckoutDate = colCheckoutDate;
    }
    public Date getColDueDate() {
        return colDueDate;
    }
    public void setColDueDate(Date colDueDate) {
        this.colDueDate = colDueDate;
    }
    public Integer getColMemberID() {
        return colMemberID;
    }
    public void setColMemberID(Integer colMemberID) {
        this.colMemberID = colMemberID;
    }
    public String getColTitle() {
        return colTitle;
    }
    public void setColTitle(String colTitle) {
        this.colTitle = colTitle;
    }
    public String getColBorrower() {
        return colBorrower;
    }
    public void setColBorrower(String colBorrower) {
        this.colBorrower = colBorrower;
    }
    public String getColCheckoutBy() {
        return colCheckoutBy;
    }
    public void setColCheckoutBy(String colCheckoutBy) {
        this.colCheckoutBy = colCheckoutBy;
    }
    public Integer getColSn() {
        return colSn;
    }
    public void setColSn(Integer colSn) {
        this.colSn = colSn;
    }

}
