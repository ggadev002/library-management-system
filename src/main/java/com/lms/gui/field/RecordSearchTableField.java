package com.lms.gui.field;

public class RecordSearchTableField {
    private String colTitle;
    private String colISBN;
    private int colId;

    public String getColTitle() {
        return colTitle;
    }
    public void setColTitle(String colTitle) {
        this.colTitle = colTitle;
    }
    public String getColISBN() {
        return colISBN;
    }
    public void setColISBN(String colISBN) {
        this.colISBN = colISBN;
    }
    public int getColId() {
        return colId;
    }
    public void setColId(int colId) {
        this.colId = colId;
    }

}
