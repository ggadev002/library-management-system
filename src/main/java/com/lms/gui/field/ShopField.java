package com.lms.gui.field;

public class ShopField {
    private Integer colId;
    private String colName;

    public Integer getColId() {
        return colId;
    }
    public void setColId(Integer colId) {
        this.colId = colId;
    }
    public String getColName() {
        return colName;
    }
    public void setColName(String colName) {
        this.colName = colName;
    }


}
