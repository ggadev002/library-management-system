package com.lms.gui;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;

public class Dashboard extends Presentation {

    public Dashboard(ScreensConfig config) {
        super(config);
    }


    @FXML
    void initialize() {
    }

    @FXML
    void goToMemberManager(ActionEvent event) {
        config.loadMemberManagerForm();
    }

    @FXML
    void goToCheckoutManager() {
        config.loadCheckoutManagerForm();
    }


    @FXML
    void goToRecordManager(ActionEvent event) {
        config.loadRecordManagerForm();
    }

    @FXML
    void goToCheckinForm(ActionEvent event) {
        config.loadCheckinForm();
    }
}
