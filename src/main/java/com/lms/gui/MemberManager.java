package com.lms.gui;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;

import com.lms.domain.Address;
import com.lms.domain.LibraryMember;
import com.lms.domain.Person;
import com.lms.gui.field.MemberField;
import com.lms.service.MemberService;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;

public class MemberManager extends Presentation {

	List<LibraryMember> memberSearchResult;
	LibraryMember selectedMember;

	@FXML
	Button btnBack, btnSearch, btnSave, btnUpdate;

	@FXML
	private TableView<MemberField> tblSearchResult;

	@FXML
	private TableColumn<MemberField, String> name;

	@FXML
    private TableColumn<MemberField, String> registrationId;

	@FXML
	private TextField idTxt, txtSearch;

	@FXML
	private TextField firstnameTxt;

	@FXML
	private TextField lastnameTxt;

	@FXML
	private TextField phoneTxt;

	@FXML
	private DatePicker dobDatePicker;

	@FXML
	private TextField streetTxt;

	@FXML
	private TextField cityTxt;

	@FXML
	private TextField stateTxt;

	@FXML
	private TextField zipTxt;

	@FXML
	private RadioButton rdbID, rdbName;

	private ToggleGroup searchByGroup;

	private static final String SEARCH_BY_ID = "ID";
	private boolean isSearchByID, isEdit = false;
	private final static SimpleDateFormat formatter = new SimpleDateFormat("MM-dd-YYYY");

	@Autowired
	private MemberService memberService;

	public MemberManager(ScreensConfig config) {
		super(config);
	}

	@FXML
	void backToDashboar() {
		config.loadDashboardForm();
	}

	@FXML
	void clearForm() {
		idTxt.clear();
		firstnameTxt.clear();
		lastnameTxt.clear();
		phoneTxt.clear();
		cityTxt.clear();
		stateTxt.clear();
		streetTxt.clear();
		zipTxt.clear();
		dobDatePicker.setValue(null);
		selectedMember = null;
	}

	String generateRegistrationId() {

		String res = "LS";
		Date date = Calendar.getInstance(Locale.US).getTime();
		int day = date.getDay();
		int month = date.getMonth();
		int year = date.getYear();

		res += day < 10 ? "0" + day : day;
		res += month < 10 ? "0" + month : month;
		res += year;

		return res;

	}

	@FXML
	void saveMember() {
	    Address address = new Address();
	    address.setCity(cityTxt.getText());
	    address.setState(stateTxt.getText());
	    address.setZip(zipTxt.getText());
	    address.setStreet(streetTxt.getText());

	    Person  person = new Person();
	    person.setAddress(address);
	    person.setDateOfBirth(fromDate(dobDatePicker.getValue()));
	    person.setFirstName(firstnameTxt.getText());
	    person.setLastName(lastnameTxt.getText());
	    person.setPhoneNumber(phoneTxt.getText());
	    LibraryMember memberData = new LibraryMember(new Date(), generateRegistrationId(), person);
		if (selectedMember == null) {
			memberService.saveMember(memberData);
		} else {
		    memberData.setId(selectedMember.getId());
			memberService.saveMember(memberData);
		}

		clearForm();
	}

	@FXML
	void initialize() {
		searchByGroup = new ToggleGroup();
		rdbID.setToggleGroup(searchByGroup);
		rdbID.setSelected(true);
		isSearchByID = true;
		rdbName.setToggleGroup(searchByGroup);
		searchByGroup.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
			@Override
			public void changed(ObservableValue<? extends Toggle> ov, Toggle t, Toggle t1) {

				RadioButton chk = (RadioButton) t1.getToggleGroup().getSelectedToggle(); // Cast
																							// object
																							// to
																							// radio
																							// button
				isSearchByID = SEARCH_BY_ID.equalsIgnoreCase(chk.getText()) ? true : false;
			}
		});

		name.setCellValueFactory(new PropertyValueFactory<MemberField, String>("name"));
		registrationId.setCellValueFactory(new PropertyValueFactory<MemberField, String>("registrationId"));
		tblSearchResult.setRowFactory(tv -> {
			TableRow<MemberField> row = new TableRow<>();
			row.setOnMouseClicked(event -> {
				if (event.getClickCount() == 2 && (!row.isEmpty())) {
					selectedMember = memberSearchResult.get(row.getIndex());
					// MemberField rowData = row.getItem();
					viewDetail();
				}
			});
			return row;
		});
	}

	private void viewDetail() {
		if (selectedMember != null) {
		    idTxt.setText(selectedMember.getRegistrationId());
	        firstnameTxt.setText(selectedMember.getInfo().getFirstName());
	        lastnameTxt.setText(selectedMember.getInfo().getLastName());
	        phoneTxt.setText(selectedMember.getInfo().getPhoneNumber());
	        cityTxt.setText(selectedMember.getInfo().getAddress().getCity());
	        stateTxt.setText(selectedMember.getInfo().getAddress().getState());
	        streetTxt.setText(selectedMember.getInfo().getAddress().getState());
	        zipTxt.setText(selectedMember.getInfo().getAddress().getZip());
	        dobDatePicker.setValue(fromDate(selectedMember.getInfo().getDateOfBirth()));
		}
	}

	private static LocalDate fromDate(Date date) {
	    Instant instant = Instant.ofEpochMilli(date.getTime());
	    return LocalDateTime.ofInstant(instant, ZoneId.systemDefault())
	        .toLocalDate();
	  }

	private static Date fromDate(LocalDate date) {
	    return Date.from(date.atStartOfDay(ZoneId.systemDefault()).toInstant());
      }

	List<MemberField> convert(List<LibraryMember> list) {
		List<MemberField> tos = new ArrayList<MemberField>();
		if (list != null) {
			for (LibraryMember s : list) {
				tos.add(convert(s));
			}
		}

		return tos;
	}

	MemberField convert(LibraryMember from) {
		MemberField to = null;
		if (null != from) {
			to = new MemberField();
			to.setName(from.getInfo().getFirstName() + " " + from.getInfo().getLastName());
			to.setPhone(from.getInfo().getPhoneNumber());
			to.setRegistrationId(from.getRegistrationId());
			to.setStreet(from.getInfo().getAddress().getStreet());
			to.setCity(from.getInfo().getAddress().getCity());
			to.setState(from.getInfo().getAddress().getState());
			to.setZip(from.getInfo().getAddress().getZip());
			to.setDob(from.getInfo().getDateOfBirth().toString());
		}

		return to;
	}

	@FXML
	void doSearch() throws Exception {

		isEdit = false;

		if (isSearchByID) {
			memberSearchResult = memberService.findByRegistrationIdContaining(txtSearch.getText().trim());
		} else {
			memberSearchResult = memberService.findByNameContaining(txtSearch.getText().trim());
		}

		if (memberSearchResult != null) {
			if (memberSearchResult.size() < 1)
				System.out.println("No Items!");
			else {

				ObservableList<MemberField> data = FXCollections.observableArrayList(convert(memberSearchResult));
				this.tblSearchResult.setItems(data);
				isEdit = true;
			}
		} /* else */
		else
			System.out.println("Error - null!");
	}

	@FXML
	void setEditable() {

	}

}
