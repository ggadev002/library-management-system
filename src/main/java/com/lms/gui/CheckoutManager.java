package com.lms.gui;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;

import com.lms.domain.Checkout;
import com.lms.domain.LibraryMember;
import com.lms.domain.User;
import com.lms.domain.collection.EBookRecord;
import com.lms.domain.collection.JournalRecord;
import com.lms.domain.collection.MediaRecord;
import com.lms.domain.collection.Record;
import com.lms.domain.collection.RecordType;
import com.lms.gui.field.RecordSearchTableField;
import com.lms.service.CheckoutService;
import com.lms.service.MemberService;
import com.lms.service.RecordService;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.paint.Color;
import javafx.util.Callback;
import javafx.util.StringConverter;

public class CheckoutManager extends Presentation {

    public CheckoutManager(ScreensConfig config) {
        super(config);
    }

    @FXML
    private TextField txtSearch;

    @FXML
    private TextField txtDueDate;

    @FXML
    private TextField txtCheckoutDate;

    @FXML
    private TextField txtISBN;

    @FXML
    private TextField txtAuthor;

    @FXML
    private TextField txtVolumeNumber;

    @FXML
    private TextField txtTitle;

    @FXML
    private TextField txtPublisher;

    @FXML
    private TextField txtEdition;

    @FXML
    private TextField txtUrl;

    @FXML
    private TextField txtRecordType;

    @FXML
    private TextField txtMediaType;

    @FXML
    private TextField txtLibraryMemberName;

    @FXML
    private ComboBox<LibraryMember> cboMember;

    @FXML
    private RadioButton rdbISBN, rdbTitle;

    @FXML
    private Button btnSearch;

    @FXML
    private Label lblStatus;

    @FXML
    private TableView<RecordSearchTableField> tblSearchResult;

    @FXML
    private TableColumn<RecordSearchTableField, String> colTitle;

    @FXML
    private TableColumn<RecordSearchTableField, String> colISBN;

    @FXML
    private TableColumn<RecordSearchTableField, Integer> colId;

    private boolean isSearchByISBN;

    private Map<Integer, Record> memorySearchResult;

    @FXML
    private Label lblRecordInfo;

    private ToggleGroup searchByGroup;
    private static final String SEARCH_BY_ISBN = "ISBN";

    @Autowired
    private RecordService recordService;

    @Autowired
    private MemberService memberService;

    @Autowired
    private CheckoutService checkoutService;

    private Record checkoutRecord;

    private final static SimpleDateFormat formatter = new SimpleDateFormat("MM-dd-YYYY");
    private Date TODAY;
    private Date NEXT_5_DAY;

    @FXML
    void initialize() {
        //initialize search by button group
        searchByGroup = new ToggleGroup();
        rdbISBN.setToggleGroup(searchByGroup);
        rdbISBN.setSelected(true);
        isSearchByISBN=true;
        rdbTitle.setToggleGroup(searchByGroup);
        searchByGroup.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
            @Override
            public void changed(ObservableValue<? extends Toggle> ov, Toggle t, Toggle t1) {

                RadioButton chk = (RadioButton)t1.getToggleGroup().getSelectedToggle(); // Cast object to radio button
                isSearchByISBN = SEARCH_BY_ISBN.equalsIgnoreCase(chk.getText()) ? true : false;
            }
        });


        //initial table
        colTitle.setCellValueFactory(new PropertyValueFactory<RecordSearchTableField, String>("colTitle"));
        colISBN.setCellValueFactory(new PropertyValueFactory<RecordSearchTableField, String>("colISBN"));
        colId.setCellValueFactory(new PropertyValueFactory<RecordSearchTableField, Integer>("colId"));
        tblSearchResult.setRowFactory( tv -> {
            TableRow<RecordSearchTableField> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
                    RecordSearchTableField rowData = row.getItem();
                    Record record = memorySearchResult.get(rowData.getColId());
                    if(record.getQty() < 1) {
                        lblRecordInfo.setText("This record is not sufficient to checkout!");
                        lblRecordInfo.setTextFill(Color.web(config.ERROR_COLOR_CODE));
                    } else {
                        lblRecordInfo.setText("");
                        viewDetail(rowData.getColId());
                    }
                }
            });
            return row ;
        });

        //init date
        txtCheckoutDate.setText(formatter.format(new Date()));
        NEXT_5_DAY = new Date(new Date().getTime() + TimeUnit.DAYS.toMillis( 5 ));
        txtDueDate.setText(formatter.format(NEXT_5_DAY));

        initLibraryMembers();
    }

    private void initLibraryMembers() {
        cboMember.setItems(FXCollections.observableArrayList(memberService.findAll()));
        cboMember.setCellFactory(new Callback<ListView<LibraryMember>, ListCell<LibraryMember>>() {
            @Override
            public ListCell<LibraryMember> call(ListView<LibraryMember> param) {
                 return new ListCell<LibraryMember>(){
                      @Override
                      public void updateItem(LibraryMember item, boolean empty){
                           super.updateItem(item, empty);
                           if(!empty) {
                            setText(item.getRegistrationId());
                           }
                      }
                 };
            }
        });

        // Define rendering of selected value shown in ComboBox.
        cboMember.setConverter(new StringConverter<LibraryMember>() {
            @Override
            public String toString(LibraryMember m) {
                if (m == null) {
                    return null;
                } else {
                    return m.getRegistrationId();
                }
            }

            @Override
            public LibraryMember fromString(String m) {
                return null;
            }
        });
    }

    @FXML
    void onSelectedMember() {
        LibraryMember member = cboMember.getSelectionModel().getSelectedItem();
        txtLibraryMemberName.setText(String.format("%s %s", member.getInfo().getFirstName(), member.getInfo().getLastName()));
    }

    @FXML
    void doSearch() {
        List<Record> searchResult = null;

        if(isSearchByISBN) {
            searchResult = recordService.searchByIsbn(txtSearch.getText().trim());
        } else {
            searchResult = recordService.searchByTitle(txtSearch.getText().trim());
        }

        updateMemorySearchResult(searchResult);
        if(searchResult != null) {
            ObservableList<RecordSearchTableField> data = FXCollections.observableArrayList(convert(searchResult));
            tblSearchResult.setItems(data);
        }
    }

    void viewDetail(int recordId) {
        checkoutRecord = memorySearchResult.get(recordId);
        String recordType = null;
        txtISBN.setText(checkoutRecord.getIsbn());
        txtAuthor.setText(checkoutRecord.getAuthors());
        txtTitle.setText(checkoutRecord.getTitle());
        txtPublisher.setText(checkoutRecord.getPublishers());
        txtEdition.setText(checkoutRecord.getEdition());

        //TODO check book quantity if it's available to checkout

        if(checkoutRecord instanceof JournalRecord) {
            txtVolumeNumber.setText(String.valueOf(((JournalRecord)checkoutRecord).getVolumeNumber()));
            recordType = RecordType.JOURNAL;
        } else if(checkoutRecord instanceof EBookRecord) {
            txtUrl.setText(((EBookRecord)checkoutRecord).getUrl());
            recordType = RecordType.EBOOK;
        } else if(checkoutRecord instanceof MediaRecord) {
            txtMediaType.setText(((MediaRecord)checkoutRecord).getType());
            recordType = RecordType.MEDIA;
        } else {
            recordType = RecordType.BOOK;
        }

        txtRecordType.setText(recordType);
    }

    @FXML
    void clearForm() {
        txtSearch.clear();
        txtISBN.clear();
        txtAuthor.clear();
        txtVolumeNumber.clear();
        txtTitle.clear();
        txtPublisher.clear();
        txtEdition.clear();
        txtUrl.clear();
        lblRecordInfo.setText("");
        txtMediaType.clear();
        txtRecordType.clear();
        txtLibraryMemberName.clear();
        lblStatus.setText("");
        cboMember.getSelectionModel().clearSelection();
        checkoutRecord = null;
        clearTable();
    }

    void clearTable() {
        final ObservableList<RecordSearchTableField> eventData = FXCollections.observableArrayList();
        eventData.removeAll(eventData);
        eventData.clear();
        tblSearchResult.setItems(eventData);
        memorySearchResult.clear();
    }

    @FXML
    void backToDashboard() {
        config.loadDashboardForm();
    }

    @FXML
    void doCheckout() {

        //assume that the record is valid to checkout
        //minus qty for record
        checkoutRecord.setQty(checkoutRecord.getQty() - 1);
        recordService.save(checkoutRecord);

        //save checkout record
        User user = config.getUserSession().getUser();
        LibraryMember borrower = cboMember.getSelectionModel().getSelectedItem();
        Checkout checkout = new Checkout(user, borrower, checkoutRecord, new Date(), NEXT_5_DAY, null);
        checkoutService.save(checkout);

        clearForm();
        lblStatus.setText("Checkout success!");
        lblStatus.setTextFill(Color.web(config.SUCCED_COLOR_CODE));
    }

    private RecordSearchTableField convert(Record from) {
        RecordSearchTableField to = null;
        if(null != from ) {
            to = new RecordSearchTableField();
            to.setColISBN(from.getIsbn());
            to.setColTitle(from.getTitle());
            to.setColId(from.getId());
        }

        return to;
    }

    private List<RecordSearchTableField> convert(List<Record> list) {
        List<RecordSearchTableField> tos = new ArrayList<RecordSearchTableField>();
        if(list != null) {
            for(Record r : list) {
                tos.add(convert(r));
            }
        }

        return tos;
    }

    private void updateMemorySearchResult(List<Record> list) {
        memorySearchResult = new HashMap<>();
        if(list != null) {
            for(Record r : list) {
                memorySearchResult.put(r.getId(), r);
            }
        }
    }
}
