package com.lms.gui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import com.lms.domain.collection.EBookRecord;
import com.lms.domain.collection.JournalRecord;
import com.lms.domain.collection.MediaRecord;
import com.lms.domain.collection.Record;
import com.lms.domain.collection.RecordFactory;
import com.lms.domain.collection.RecordType;
import com.lms.gui.field.RecordSearchTableField;
import com.lms.service.RecordService;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.paint.Color;

public class RecordManager extends Presentation {

    public RecordManager(ScreensConfig config) {
        super(config);
    }

    @FXML
    private TextField txtSearch;

    @FXML
    private TextField txtId;

    @FXML
    private TextField txtISBN;

    @FXML
    private TextField txtAuthor;

    @FXML
    private TextField txtVolumeNumber;

    @FXML
    private TextField txtTitle;

    @FXML
    private TextField txtPublisher;

    @FXML
    private TextField txtEdition;

    @FXML
    private TextField txtUrl;

    @FXML
    private ComboBox<String> cboRecordType;

    @FXML
    private ComboBox<String> cboMediaType;

    @FXML
    private RadioButton rdbISBN, rdbTitle;

    @FXML
    private Button btnSearch;

    @FXML
    private Label lblStatus;

    @FXML
    private TextField txtQty;

    @FXML
    private TableView<RecordSearchTableField> tblSearchResult;

    @FXML
    private TableColumn<RecordSearchTableField, String> colTitle;

    @FXML
    private TableColumn<RecordSearchTableField, String> colISBN;

    @FXML
    private TableColumn<RecordSearchTableField, Integer> colId;

    private boolean isSearchByISBN;

    private Map<Integer, Record> memorySearchResult;

    private ToggleGroup searchByGroup;
    private static final String SEARCH_BY_ISBN = "ISBN";

    @Autowired
    private RecordService recordService;

    @FXML
    void initialize() {
        //initialize search by button group
        searchByGroup = new ToggleGroup();
        rdbISBN.setToggleGroup(searchByGroup);
        rdbISBN.setSelected(true);
        isSearchByISBN=true;
        rdbTitle.setToggleGroup(searchByGroup);
        searchByGroup.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
            @Override
            public void changed(ObservableValue<? extends Toggle> ov, Toggle t, Toggle t1) {

                RadioButton chk = (RadioButton)t1.getToggleGroup().getSelectedToggle(); // Cast object to radio button
                isSearchByISBN = SEARCH_BY_ISBN.equalsIgnoreCase(chk.getText()) ? true : false;
            }
        });

        //initialize combo box
        cboMediaType.setItems(FXCollections.observableArrayList(
                    MediaRecord.AUDIO, MediaRecord.VIDEO
                ));

        cboRecordType.setItems(FXCollections.observableArrayList(
                RecordType.BOOK, RecordType.EBOOK, RecordType.JOURNAL, RecordType.MEDIA
            ));

        txtId.setStyle(config.DISABLED_CSS_STYPE);

        //initial table
        colTitle.setCellValueFactory(new PropertyValueFactory<RecordSearchTableField, String>("colTitle"));
        colISBN.setCellValueFactory(new PropertyValueFactory<RecordSearchTableField, String>("colISBN"));
        colId.setCellValueFactory(new PropertyValueFactory<RecordSearchTableField, Integer>("colId"));
        tblSearchResult.setRowFactory( tv -> {
            TableRow<RecordSearchTableField> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
                    RecordSearchTableField rowData = row.getItem();
                    viewDetail(rowData.getColId());
                }
            });
            return row ;
        });
    }


    @FXML
    void doSearch() {
        List<Record> searchResult = null;

        if(isSearchByISBN) {
            searchResult = recordService.searchByIsbn(txtSearch.getText().trim());
        } else {
            searchResult = recordService.searchByTitle(txtSearch.getText().trim());
        }

        updateMemorySearchResult(searchResult);
        if(searchResult != null) {
            ObservableList<RecordSearchTableField> data = FXCollections.observableArrayList(convert(searchResult));
            tblSearchResult.setItems(data);
        }
    }

    void viewDetail(int recordId) {
        Record record = memorySearchResult.get(recordId);
        String recordType = null;
        txtId.setText(String.valueOf(record.getId()));
        txtISBN.setText(record.getIsbn());
        txtAuthor.setText(record.getAuthors());
        txtTitle.setText(record.getTitle());
        txtPublisher.setText(record.getPublishers());
        txtEdition.setText(record.getEdition());
        txtQty.setText(String.valueOf(record.getQty()));

        if(record instanceof JournalRecord) {
            txtVolumeNumber.setText(String.valueOf(((JournalRecord)record).getVolumeNumber()));
            recordType = RecordType.JOURNAL;

        } else if(record instanceof EBookRecord) {
            txtUrl.setText(((EBookRecord)record).getUrl());
            recordType = RecordType.EBOOK;
        } else if(record instanceof MediaRecord) {
            cboMediaType.setValue(((MediaRecord)record).getType());
            recordType = RecordType.MEDIA;
        } else {
            recordType = RecordType.BOOK;
        }

        cboRecordType.setValue(recordType);
    }

    @FXML
    void clearForm() {
        txtSearch.clear();
        txtId.clear();
        txtISBN.clear();
        txtAuthor.clear();
        txtVolumeNumber.clear();
        txtTitle.clear();
        txtPublisher.clear();
        txtEdition.clear();
        txtUrl.clear();
        txtQty.clear();
        lblStatus.setText("");
        txtTitle.requestFocus();
    }

    @FXML
    void backToDashboard() {
        config.loadDashboardForm();
    }

    @FXML
    void saveRecord() {
        //check Record type first and assign correct class
        Record record = null;

        if(RecordType.BOOK.equals(cboRecordType.getValue())) {
                record = RecordFactory.createBook(txtISBN.getText(), txtTitle.getText(), txtAuthor.getText()
                        , txtPublisher.getText(), txtEdition.getText(), Integer.valueOf(txtQty.getText()));
        } else if(RecordType.EBOOK.equals(cboRecordType.getValue())) {
                record = RecordFactory.createEBook(txtISBN.getText(), txtTitle.getText(), txtAuthor.getText()
                        , txtPublisher.getText(), txtEdition.getText(), Integer.valueOf(txtQty.getText()),
                        txtUrl.getText());
        } else if(RecordType.JOURNAL.equals(cboRecordType.getValue())) {
                record = RecordFactory.createJournal(txtISBN.getText(), txtTitle.getText(), txtAuthor.getText()
                        , txtPublisher.getText(), txtEdition.getText(), Integer.valueOf(txtQty.getText()),
                        txtVolumeNumber.getText());
        } else if(RecordType.MEDIA.equals(cboRecordType.getValue())) {
                record = RecordFactory.createMedia(txtISBN.getText(), txtTitle.getText(), txtAuthor.getText()
                        , txtPublisher.getText(), txtEdition.getText(), Integer.valueOf(txtQty.getText()),
                        cboMediaType.getValue());
        }

        if(record != null) {
            try {
                // set Id if the record already exist
                boolean oldRecor = !StringUtils.isEmpty(txtId.getText());
                if(oldRecor) {
                    record.setId(Integer.valueOf(txtId.getText()));
                }

                // update search result object
                Record savedRecord = recordService.save(record);
                if(oldRecor) {
                    memorySearchResult.put(record.getId(), savedRecord);
                }

                //display success text
                lblStatus.setText("Record saved!");
                lblStatus.setTextFill(Color.web(config.SUCCED_COLOR_CODE));
                clearForm();
            } catch(Exception ex) {
                lblStatus.setTextFill(Color.web(config.ERROR_COLOR_CODE));
                lblStatus.setText("Something went wrong, please try again.");
            }
        }

    }

    private RecordSearchTableField convert(Record from) {
        RecordSearchTableField to = null;
        if(null != from ) {
            to = new RecordSearchTableField();
            to.setColISBN(from.getIsbn());
            to.setColTitle(from.getTitle());
            to.setColId(from.getId());
        }

        return to;
    }

    private List<RecordSearchTableField> convert(List<Record> list) {
        List<RecordSearchTableField> tos = new ArrayList<RecordSearchTableField>();
        if(list != null) {
            for(Record r : list) {
                tos.add(convert(r));
            }
        }

        return tos;
    }

    private void updateMemorySearchResult(List<Record> list) {
        memorySearchResult = new HashMap<>();
        if(list != null) {
            for(Record r : list) {
                memorySearchResult.put(r.getId(), r);
            }
        }
    }
}
