package com.lms.gui;

import com.lms.domain.User;

public final class UserSession {
    private final User user;

    public UserSession(User user) {
        this.user = user;
    }
    
    public User getUser() {
        return this.user;
    }
}
