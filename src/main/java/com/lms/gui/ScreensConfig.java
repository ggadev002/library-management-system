package com.lms.gui;

import java.net.URL;
import java.util.Observable;
import java.util.Observer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;

import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.util.Callback;

@Configuration
@Lazy
public class ScreensConfig implements Observer {
    private static final Logger logger = LogManager.getLogger(ScreensConfig.class);

    public static final int WIDTH = 927;
    public static final int HEIGHT = 500;
    public static final String STYLE_FILE = "application.css";
    public static final String SUCCED_COLOR_CODE = "#5cb85c";
    public static final String ERROR_COLOR_CODE = "#d9534f";
    public static final String DISABLED_COLOR_CODE = "#C1C1BE";
    public static final String DISABLED_CSS_STYPE = "-fx-background-color : #C1C1BE";
    private UserSession userSession;

    private Stage stage;
    private Scene scene;
    private StackPane root;

    public void setPrimaryStage(Stage primaryStage) {
        this.stage = primaryStage;
    }

    public void showMainScreen() {
        root = new StackPane();
        root.getStylesheets().add(STYLE_FILE);
        root.getStyleClass().add("main-window");
        stage.setTitle("Library Management System");
        scene = new Scene(root, WIDTH, HEIGHT);
        stage.setScene(scene);
        stage.setResizable(false);

        stage.setOnHiding(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
                System.exit(0);
            }
        });

        stage.show();
    }

    private void setNode(Node node) {
        root.getChildren().setAll(node);
    }

    private void setNodeOnTop(Node node) {
        root.getChildren().add(node);
    }

    public void removeNode(Node node) {
        root.getChildren().remove(node);
    }

    void loadLoginForm() {
        setNode(getNode(loginForm(), getClass().getResource("Login.fxml")));
    }

    void loadDashboardForm() {
        setNode(getNode(dashboardForm(), getClass().getResource("Dashboard.fxml")));
    }

    void loadRecordManagerForm() {
        setNode(getNode(recordManagerForm(), getClass().getResource("RecordManager.fxml")));
    }

    void loadCheckinForm() {
        setNode(getNode(checkinForm(), getClass().getResource("CheckinManager.fxml")));
    }

    void loadMemberManagerForm() {
        setNode(getNode(memberManagerForm(), getClass().getResource("MemberManager.fxml")));
    }

    void loadCheckoutManagerForm() {
        setNode(getNode(checkoutManagerForm(), getClass().getResource("CheckoutManager.fxml")));
    }

    @Bean
    @Scope("prototype")
    Login loginForm() {
        return new Login(this);
    }

    @Bean
    @Scope("prototype")
    Dashboard dashboardForm(){
        return new Dashboard(this);
    }

    @Bean
    @Scope("prototype")
    CheckinManager checkinForm(){
        return new CheckinManager(this);
    }

    @Bean
    @Scope("prototype")
    CheckoutManager checkoutManagerForm(){
        return new CheckoutManager(this);
    }

    @Bean
    @Scope("prototype")
    RecordManager recordManagerForm(){
        return new RecordManager(this);
    }

    @Bean
    @Scope("prototype")
    MemberManager memberManagerForm(){
        return new MemberManager(this);
    }

    private Node getNode(final Presentation control, URL location) {
        FXMLLoader loader = new FXMLLoader(location, null);
        loader.setControllerFactory(new Callback<Class<?>, Object>() {
            @Override
            public Object call(Class<?> aClass) {
                return control;
            }
        });

        try {
            return (Node) loader.load();
        } catch (Exception e) {
            logger.error("Error casting node", e);
            return null;
        }
    }

    public Stage getStage() {
        return stage;
    }

    @Override
    public void update(Observable o, Object arg) {
        loadLoginForm();
    }

    public UserSession getUserSession() {
        return userSession;
    }

    public void setUserSession(UserSession userSession) {
        this.userSession = userSession;
    }
}
