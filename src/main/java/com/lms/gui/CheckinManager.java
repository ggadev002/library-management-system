package com.lms.gui;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.lms.domain.Checkout;
import com.lms.domain.collection.Record;
import com.lms.gui.field.CheckoutField;
import com.lms.service.RecordService;
import com.lms.service.ReturnService;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.paint.Color;

public class CheckinManager extends Presentation {

	@Autowired
	private ReturnService returnService;

	@FXML
	private TextField memberIdField;

	@FXML
	private Button searchButton;

	@FXML
	private Button returnButton;

	List<Checkout> checkoutItems;

	@FXML
	private Label lblStatus;

	@FXML
	private TableColumn<CheckoutField, Integer> colSn;

	@FXML
	private TableView<CheckoutField> borrowTableView;

	@FXML
	private TableColumn<CheckoutField, String> colTitle;

	@FXML
    private TableColumn<CheckoutField, String> colCheckoutBy;

	@FXML
    private TableColumn<CheckoutField, String> colBorrower;

	@FXML
	private TableColumn<CheckoutField, String> colCheckoutDate;

	@FXML
	private TableColumn<CheckoutField, String> colDueDate;

	@Autowired
	private RecordService recordService;


	public CheckinManager(ScreensConfig config) {
		super(config);
	}

	@FXML
	void returnItem() {
		int selectedIndex = borrowTableView.getSelectionModel().getSelectedIndex();
		if (checkoutItems != null && selectedIndex > -1) {

			Checkout checkoutItem = checkoutItems.get(selectedIndex);
			checkoutItem.setReturnDate(Calendar.getInstance().getTime());

			//update record
			Record record = checkoutItem.getRecord();
			record.setQty(checkoutItem.getRecord().getQty() + 1);
			checkoutItem.setRecord(recordService.save(record));

			returnService.save(checkoutItem);
			borrowTableView.getItems().remove(selectedIndex);
			lblStatus.setText("Return item succed!");
	        lblStatus.setTextFill(Color.web(config.SUCCED_COLOR_CODE));
		}
	}


	@FXML
	void doSearch() {

		try {
			checkoutItems = returnService.findBorrowedItems(memberIdField.getText().trim());
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		if (checkoutItems != null) {
			if (checkoutItems.size() < 1)
				System.out.println("No Items!");
			else{

				ObservableList<CheckoutField> data = FXCollections.observableArrayList(convert(checkoutItems));
				borrowTableView.setItems(data);
			}
		} /*else */
		else
			System.out.println("Error - null!");

	}

	List<CheckoutField> convert(List<Checkout> list) {
		List<CheckoutField> tos = new ArrayList<CheckoutField>();
		if (list != null) {
		    int i=0;
			for (Checkout s : list) {
			    CheckoutField to = convert(s);
			    to.setColSn(++i);
				tos.add(to);
			}
		}

		return tos;
	}

	CheckoutField convert(Checkout from) {
		CheckoutField to = null;
		if (null != from) {
			to = new CheckoutField();
			to.setColCheckoutDate(from.getCheckoutDate());
			to.setColCheckoutId(from.getId());
			to.setColDueDate(from.getDueDate());
			to.setColTitle(from.getRecord().getTitle());
			to.setColMemberID(from.getBorrower().getId());
			to.setColBorrower(from.getBorrower().getInfo().getFirstName() + from.getBorrower().getInfo().getLastName());
			to.setColCheckoutBy(from.getUser().getUsername());
		}

		return to;
	}

	@FXML
    void backToDashboard() {
        config.loadDashboardForm();
    }

	@FXML
	void initialize() {
		colTitle.setCellValueFactory(new PropertyValueFactory<CheckoutField, String>("colTitle"));
		colDueDate.setCellValueFactory(new PropertyValueFactory<CheckoutField, String>("colDueDate"));
		colCheckoutDate.setCellValueFactory(new PropertyValueFactory<CheckoutField, String>("colCheckoutDate"));
		colCheckoutBy.setCellValueFactory(new PropertyValueFactory<CheckoutField, String>("colCheckoutBy"));
		colBorrower.setCellValueFactory(new PropertyValueFactory<CheckoutField, String>("colBorrower"));
		colSn.setCellValueFactory(new PropertyValueFactory<CheckoutField, Integer>("colSn"));
	}

}
