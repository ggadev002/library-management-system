package com.lms.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.lms.domain.collection.Record;

@Entity
@Table(name="tblCheckout")
public class Checkout {
    @GeneratedValue
    @Id
    @Column(name = "checkoutId")
    private int id;

    @OneToOne
    @JoinColumn(name="userId")
    private User user;

    @OneToOne
    @JoinColumn(name="libraryMemberId")
    private LibraryMember borrower;

    @OneToOne
    @JoinColumn(name="recordId")
    private Record record;
    private Date checkoutDate;
    private Date dueDate;
    private Date returnDate;

    public Checkout(){}

    public Checkout(User user, LibraryMember borrower, Record record,
            Date checkoutDate, Date dueDate, Date returnDate) {
        super();
        this.user = user;
        this.borrower = borrower;
        this.record = record;
        this.checkoutDate = checkoutDate;
        this.dueDate = dueDate;
        this.returnDate = returnDate;
    }
    public User getUser() {
        return user;
    }
    public void setUser(User user) {
        this.user = user;
    }
    public LibraryMember getBorrower() {
        return borrower;
    }
    public void setBorrower(LibraryMember borrower) {
        this.borrower = borrower;
    }
    public Record getRecord() {
        return record;
    }
    public void setRecord(Record record) {
        this.record = record;
    }
    public Date getCheckoutDate() {
        return checkoutDate;
    }
    public void setCheckoutDate(Date checkoutDate) {
        this.checkoutDate = checkoutDate;
    }
    public Date getDueDate() {
        return dueDate;
    }
    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }
    public Date getReturnDate() {
        return returnDate;
    }
    public void setReturnDate(Date returnDate) {
        this.returnDate = returnDate;
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

}
