package com.lms.domain;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;


@Entity
@Table(name = "tblUser")
public class User {

    @GeneratedValue
    @Id
    @Column(name = "userId")
    private int id;
    private String username;
    private String password;

    // The is true if the user has role as admin, and the admin user will be initialized when the system is created
    private boolean isAdmin;

    @OneToOne
    @JoinColumn(name="personId")
    private Person userInfo;

    private Date lastLoginDate;

    public User(){}

    public User(String username, String password, Person userInfo, Date lastLoginDate) {
        this.username = username;
        this.password = password;
        this.userInfo = userInfo;
        this.setLastLoginDate(lastLoginDate);
    }

    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public Person getUserInfo() {
        return userInfo;
    }
    public void setUserInfo(Person userInfo) {
        this.userInfo = userInfo;
    }

    public Date getLastLoginDate() {
        return lastLoginDate;
    }

    public void setLastLoginDate(Date lastLoginDate) {
        this.lastLoginDate = lastLoginDate;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean isAdmin) {
        this.isAdmin = isAdmin;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

}
