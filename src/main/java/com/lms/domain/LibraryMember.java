package com.lms.domain;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="tblLibraryMember")
public class LibraryMember {
    @GeneratedValue
    @Id
    @Column(name = "libraryMemberId")
    private int id;
    private Date registrationDate;
    private String registrationId;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="personId")
    private Person info;

    public LibraryMember(){}

    public LibraryMember(Date registrationDate, String registrationId,
            Person info) {
        this.registrationDate = registrationDate;
        this.registrationId = registrationId;
        this.info = info;
    }

    public Date getRegistrationDate() {
        return registrationDate;
    }
    public void setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
    }
    public String getRegistrationId() {
        return registrationId;
    }
    public void setRegistrationId(String registrationId) {
        this.registrationId = registrationId;
    }
    public Person getInfo() {
        return info;
    }
    public void setInfo(Person info) {
        this.info = info;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

}
