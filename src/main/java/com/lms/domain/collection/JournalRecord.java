package com.lms.domain.collection;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="tblRecord")
@DiscriminatorValue(RecordType.JOURNAL)
public class JournalRecord extends Record {
    private String volumeNumber;

    JournalRecord(String isbn, String title, String authors,
            String publishers, String edition, int qty, String volumeNumber) {
        super(isbn, title, authors, publishers, edition, qty);
        this.volumeNumber = volumeNumber;
    }

    JournalRecord() {
        super();
    }

    public String getVolumeNumber() {
        return volumeNumber;
    }

    public void setVolumeNumber(String volumeNumber) {
        this.volumeNumber = volumeNumber;
    }
}
