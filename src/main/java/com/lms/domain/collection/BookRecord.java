package com.lms.domain.collection;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="tblRecord")
@DiscriminatorValue(RecordType.BOOK)
public class BookRecord extends Record {

    BookRecord(String isbn, String title, String authors,
            String publishers, String edition, int qty) {
        super(isbn, title, authors, publishers, edition, qty);
    }

    BookRecord() {
        super();
    }

}
