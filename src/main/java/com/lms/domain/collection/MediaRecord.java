package com.lms.domain.collection;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="tblRecord")
@DiscriminatorValue(RecordType.MEDIA)
public class MediaRecord extends Record {
    public static final String VIDEO = "VIDEO";
    public static final String AUDIO = "AUDIO";

    private String type;

    MediaRecord(String isbn, String title, String authors,
            String publishers, String edition, int qty, String type) {
        super(isbn, title, authors, publishers, edition, qty);
        this.type = type;
    }

    MediaRecord() {
        super();
    }

    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }
}
