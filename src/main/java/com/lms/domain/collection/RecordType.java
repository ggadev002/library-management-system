package com.lms.domain.collection;

public class RecordType {
    public static final String BOOK = "BOOK";
    public static final String EBOOK = "EBOOK";
    public static final String MEDIA = "MEDIA";
    public static final String JOURNAL = "JOURNAL";
}
