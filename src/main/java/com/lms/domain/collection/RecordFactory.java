package com.lms.domain.collection;

public final class RecordFactory {

    public static JournalRecord createJournal(String isbn, String title, String authors,
            String publishers, String edition, int qty, String volumeNumber) {
        return new JournalRecord(isbn, title, authors, publishers, edition, qty, volumeNumber);
    }

    public static MediaRecord createMedia(String isbn, String title, String authors,
            String publishers, String edition, int qty, String type) {
        return new MediaRecord(isbn, title, authors, publishers, edition, qty, type);
    }

    public static EBookRecord createEBook(String isbn, String title, String authors,
            String publishers, String edition, int qty, String url) {
        return new EBookRecord(isbn, title, authors, publishers, edition, qty, url);
    }

    public static BookRecord createBook(String isbn, String title, String authors,
            String publishers, String edition, int qty) {
        return new BookRecord(isbn, title, authors, publishers, edition, qty);
    }

}
