package com.lms.domain.collection;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;


@Entity
@Table(name = "tblRecord")
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(
   name="recordType",
   discriminatorType=DiscriminatorType.STRING
)
@DiscriminatorValue(value="EMPTY")
public abstract class Record {

    @Id
    @GeneratedValue
    @Column(name = "recordId")
    private int id;

    protected String isbn;
    protected String title;

    protected String authors;
    protected String publishers;
    private String edition;
    private int qty;

    Record(String isbn, String title, String authors,
            String publishers, String edition, int qty) {
        this.isbn = isbn;
        this.title = title;
        this.authors = authors;
        this.publishers = publishers;
        this.edition = edition;
        this.qty = qty;
    }
    public Record() {
    }

    public String getIsbn() {
        return isbn;
    }
    public void setISBN(String isbn) {
        this.isbn = isbn;
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthors() {
		return authors;
	}
	public void setAuthors(String authors) {
		this.authors = authors;
	}
	public String getPublishers() {
		return publishers;
	}
	public void setPublishers(String publishers) {
		this.publishers = publishers;
	}
	public String getEdition() {
        return edition;
    }
    public void setEdition(String edition) {
        this.edition = edition;
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public int getQty() {
        return qty;
    }
    public void setQty(int qty) {
        this.qty = qty;
    }

}
