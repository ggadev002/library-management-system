package com.lms.domain.collection;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="tblRecord")
@DiscriminatorValue(RecordType.EBOOK)
public class EBookRecord extends BookRecord {
    private String url;

    EBookRecord(String isbn, String title, String authors,
            String publishers, String edition, int qty, String url) {
        super(isbn, title, authors, publishers,  edition, qty);
        this.url = url;
    }

    EBookRecord() {
        super();
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
