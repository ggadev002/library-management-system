Prepare by 
Group 2
 - Vandet Pin
 - Olusegun F. Abimbola

# Library Management System #

### Prerequisites ###
* Install MySQL
* DB structure can be found in sql/init-db.sql  
* Set mysql username, password, and db-name in  src/main/resources/application.properties

* [Download Maven](https://maven.apache.org/download.cgi)
* Ensure JAVA_HOME environment variable is set
* Ensure PATH environment variable contains MAVEN_HOME/bin
* Run `mvn clean install` in the project root to build the entire project

### Running the Application###
* In the root folder, enter the command `mvn jfx:run`
* or you can import project to Eclipse IDE and Run as Java application


### Technology and framework ###
* Java8
* JavaFX
* Spring
* Spring Data JPA + Hibernate
* MySQL
* Maven
