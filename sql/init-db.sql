/*
SQLyog Ultimate v8.55 
MySQL - 5.6.17 : Database - lms-db
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`lms-db` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;

USE `lms-db`;

/*Table structure for table `tblcheckout` */

DROP TABLE IF EXISTS `tblcheckout`;

CREATE TABLE `tblcheckout` (
  `checkoutId` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `libraryMemberId` int(11) NOT NULL,
  `recordId` int(11) NOT NULL,
  `checkoutDate` date DEFAULT NULL,
  `dueDate` date DEFAULT NULL,
  `returnDate` date DEFAULT NULL,
  PRIMARY KEY (`checkoutId`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `tblcheckout` */

insert  into `tblcheckout`(`checkoutId`,`userId`,`libraryMemberId`,`recordId`,`checkoutDate`,`dueDate`,`returnDate`) values (3,1,1,1,'2016-06-02','2016-06-07','2016-06-03'),(6,1,1,1,'2016-06-03','2016-06-08','2016-06-03'),(7,1,2,1,'2016-06-03','2016-06-08','2016-06-03'),(8,1,1,5,'2016-06-03','2016-06-08','2016-06-03'),(9,1,4,4,'2016-06-03','2016-06-08','2016-06-03'),(10,1,1,6,'2016-06-03','2016-06-08',NULL),(11,1,2,5,'2016-06-03','2016-06-08','2016-06-03'),(12,1,2,4,'2016-06-03','2016-06-08','2016-06-03');

/*Table structure for table `tbllibrarymember` */

DROP TABLE IF EXISTS `tbllibrarymember`;

CREATE TABLE `tbllibrarymember` (
  `libraryMemberId` int(11) NOT NULL AUTO_INCREMENT,
  `registrationDate` timestamp NULL DEFAULT NULL,
  `registrationId` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `personId` int(11) NOT NULL,
  PRIMARY KEY (`libraryMemberId`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `tbllibrarymember` */

insert  into `tbllibrarymember`(`libraryMemberId`,`registrationDate`,`registrationId`,`personId`) values (1,'2016-06-05 15:23:49','LS0005116',9),(2,NULL,'12301',2),(3,NULL,'12302',4),(4,NULL,'12303',5),(5,NULL,'12304',3),(6,NULL,'12305',6),(7,'2016-06-05 15:24:56','LS0005116',10);

/*Table structure for table `tblperson` */

DROP TABLE IF EXISTS `tblperson`;

CREATE TABLE `tblperson` (
  `personId` int(11) NOT NULL AUTO_INCREMENT,
  `firstName` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastName` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dateOfBirth` date DEFAULT NULL,
  `phoneNumber` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `street` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zip` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`personId`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `tblperson` */

insert  into `tblperson`(`personId`,`firstName`,`lastName`,`dateOfBirth`,`phoneNumber`,`street`,`city`,`state`,`zip`) values (1,'Vandet','Pin','1988-06-03','012222 90099','90','Fairfield','IOWA','5000'),(2,'Olusegun','Abmbola','1987-06-03','090434 34871','20','Fairfield','IOWA','5001'),(3,'James','Bond','1970-06-03','082322 34871','100','Cedar Rapid','IOWA','3000'),(4,'Steve ','Jobs','1955-06-03','082322 92032','40','San Francisco','California','2000'),(5,'Sujan','Shrestha','1993-06-03','090434 34871','11','Fairfield','IOWA','5001'),(6,'Nishant','Balami','1992-06-03','082322 92032','11','Fairfield','IOWA','5001'),(7,'Vandet1','Pin','1988-06-03','012222 90099','IOWA','Fairfield','IOWA','50001'),(8,'Vandet1','Pin','1988-06-03','012222 90099','IOWA','Fairfield','IOWA','50001'),(9,'Vandet','Pin','1988-06-03','012222 90099','IOWA','Fairfield','IOWA','50001'),(10,'Jimmy','3rd','2016-06-02','239023 023','90','Phnom Penh','N/A','200');

/*Table structure for table `tblrecord` */

DROP TABLE IF EXISTS `tblrecord`;

CREATE TABLE `tblrecord` (
  `recordId` int(11) NOT NULL AUTO_INCREMENT,
  `isbn` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `authors` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `publishers` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `edition` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `qty` int(11) NOT NULL DEFAULT '0',
  `url` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `volumeNumber` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'VIDEO, AUDIO',
  `recordType` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`recordId`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `tblrecord` */

insert  into `tblrecord`(`recordId`,`isbn`,`title`,`authors`,`publishers`,`edition`,`qty`,`url`,`volumeNumber`,`type`,`recordType`) values (1,'159463193X','The Kite Runner','Khaled Hosseini','Riverhead Books','1008',1,'',NULL,NULL,'EBOOK'),(2,'0321356683','Effective Java','Joshua J. Bloch','Addison-Wesley','6',1,NULL,NULL,NULL,'BOOK'),(3,'82392382','ABC','Segun','Vandet','300',0,NULL,NULL,'VIDEO','MEDIA'),(4,'0321349601','Java Concurrency in Practice','Brian Goetz','Addison-Wesley Professional','1',1,NULL,NULL,NULL,'BOOK'),(5,'1935182358','Spring in Action','Craig Walls','Manning Publications','3',1,NULL,NULL,NULL,'BOOK'),(6,'0321356683','Effective Java','Joshua J. Bloch','Addison-Wesley','5',0,NULL,NULL,'VIDEO','MEDIA');

/*Table structure for table `tbluser` */

DROP TABLE IF EXISTS `tbluser`;

CREATE TABLE `tbluser` (
  `userId` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `isAdmin` int(1) NOT NULL DEFAULT '0',
  `personId` int(11) DEFAULT NULL,
  `lastLoginDate` date DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `tbluser` */

insert  into `tbluser`(`userId`,`username`,`password`,`isAdmin`,`personId`,`lastLoginDate`) values (1,'admin','admin',1,NULL,NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
